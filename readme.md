<h2>Verjaardag - simple rsvp</h2>

<h3>Install</h3>
- install docker
- install docker-compose
- sudo apt-get install ruby
- gem install dory
- dory up
- docker-compose up -d web

Site:
http://web.verjaardag.docker

Mailcatcher:
http://mailcatcher.verjaardag.docker

Mysql:
mysql.verjaardag.docker:3306