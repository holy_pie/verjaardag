#!/usr/bin/env bash

set -xe

# Install phpunit
curl -Lo /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install deployer
wget http://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep

# Install Composer
curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer