FROM php:7.1-apache

RUN apt-get update && apt-get install -y \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    vim \
    libfreetype6-dev \
    g++ \
    libxml2-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libgmp-dev \
    libxml2-dev \
    git \
    zlib1g-dev \
    sudo \
    libicu-dev \
    subversion

RUN cd /tmp && \
    svn checkout http://source.icu-project.org/repos/icu/icu/tags/release-55-1/ icu && \
    cd icu/source && \
    ./configure --prefix=/tmp/icubuild && \
    make && \
    make install && \
    docker-php-ext-configure intl --with-icu-dir=/tmp/icubuild && \
    docker-php-ext-install intl

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/lib \
    && docker-php-ext-install gd exif \
    && docker-php-ext-install soap \
    && docker-php-ext-install mcrypt \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install zip \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && docker-php-ext-install bcmath

RUN usermod -u 1000 www-data
RUN touch /usr/local/etc/php/php.ini
RUN echo "date.timezone = Europe/Amsterdam" >  /usr/local/etc/php/php.ini
RUN echo "short_open_tag = Off" >>  /usr/local/etc/php/php.ini

RUN sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/web/g' /etc/apache2/sites-available/000-default.conf
RUN sed -i 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf

RUN echo "opcache.memory_consumption=128\n" > /usr/local/etc/php/conf.d/opcache-recommended.ini \
	&& echo "opcache.interned_strings_buffer=8\n" >> /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && echo "opcache.max_accelerated_files=4000\n" >> /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && echo "opcache.revalidate_freq=0\n" >> /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && echo "opcache.fast_shutdown=1\n" >> /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && echo "opcache.enable_cli=1\n" >> /usr/local/etc/php/conf.d/opcache-recommended.ini


RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=true" >> /usr/local/etc/php/conf.d/xdebug.ini

RUN sed -i '1 a xdebug.remote_port=9000' /usr/local/etc/php/conf.d/xdebug.ini \
    && sed -i '1 a xdebug.remote_connect_back=on' /usr/local/etc/php/conf.d/xdebug.ini \
    && sed -i '1 a xdebug.remote_autostart=on' /usr/local/etc/php/conf.d/xdebug.ini

RUN a2enmod headers expires deflate rewrite

RUN mkdir -p /var/www/html/vendor && chown www-data:www-data -R /var/www

RUN apt-get install -y ruby ruby-dev wget && gem install compass

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

CMD sh scripts/run.sh && apache2-foreground
