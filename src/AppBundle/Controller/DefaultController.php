<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Registration;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template("AppBundle::index.html.twig")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
     */
    public function indexAction(Request $request)
    {
        $reg = new Registration();

        $form = $this->createFormBuilder($reg)
            ->add('name', TextType::class, array('required' => true, 'label' => 'Naam'))
            ->add('yes', SubmitType::class, array('label' => 'Ja ik kom'))
            ->add('no', SubmitType::class, array(
                'label' => 'Nee ik kan niet',
                'attr'  => array('class' => 'btn-default pull-right')
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var SubmitButton $yesButton */
            $yesButton = $form->get('yes');
            if ($yesButton->isClicked()) {
                $reg->setAnswer('ja');
            } else {
                $reg->setAnswer('nee');
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($reg);
            $em->flush();
            return $this->redirectToRoute('thanks');
        }

        return [
            'form' => $form->createView(),
        ];

    }

    /**
     * @Route("/bedankt", name="thanks")
     */
    public function thanksAction()
    {
        return $this->render('AppBundle::thanks.html.twig', array());
    }
}
